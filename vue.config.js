module.exports = {
  publicPath: './',
  chainWebpack: config=>{
    // config 配置对象
    // when 相当于if 参数1：条件  参数2：函数
    // 条件成立，函数执行，反之函数不执行
    // 判断开发阶段和发布阶段

    // 开发阶段
    config.when(process.env.NODE_ENV === 'development', options=>{
      options.entry('app').clear().add('./src/main-dev.js')
      //给 htmlWebpackPlugin添加标识
      config.plugin('html').tap(args=>{
        //添加参数isProd
        args[0].isProd = false
        return args
      })
    })

    // 发布阶段
    config.when(process.env.NODE_ENV === 'production', options=>{
      options.entry('app').clear().add('./src/main-prod.js')
      //使用externals设置排除项
      config.set('externals',{
        vue:'Vue',
        'vue-router':'VueRouter',
        axios:'axios',
        echarts:'echarts',
        nprogress:'NProgress',
        'vue-quill-editor':'VueQuillEditor'
      })

      //给 htmlWebpackPlugin添加标识
      config.plugin('html').tap(args=>{
        //添加参数isProd
        args[0].isProd = true
        return args
      })
    })
  }
}