var pluginsArr = []
// "transform-remove-console"
// 什么时候添加"transform-remove-console"
if(process.env.NODE_ENV === 'production') {
  pluginsArr.push("transform-remove-console")
}
module.exports = {
  'presets': [
    '@vue/app'
  ],
  'plugins': [
    [
      'component',
      {
        'libraryName': 'element-ui',
        'styleLibraryName': 'theme-chalk'
      }
    ],
    ...pluginsArr,
    "@babel/plugin-syntax-dynamic-import"
  ]
}
