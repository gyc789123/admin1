import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/element.js'
// 导入字体图标
import './assets/fonts/iconfont.css'
// 导入全局样式表
import './assets/css/global.css'
import axios from 'axios'
import ZkTable from 'vue-table-with-tree-grid'
import VueQuillEditor from 'vue-quill-editor'

import 'quill/dist/quill.core.css' // import styles
import 'quill/dist/quill.snow.css' // for snow theme
import 'quill/dist/quill.bubble.css' // for bubble theme
//导入进度条插件
import NProgress from 'nprogress'
//导入进度条样式
import 'nprogress/nprogress.css'

Vue.use(VueQuillEditor, /* { default global options } */)
// 配置根域名
axios.defaults.baseURL = 'https://www.liulongbin.top:8888/api/private/v1'
// axios.defaults.baseURL = 'http://127.0.0.1:8888/api/private/v1/'
// axios请求拦截器
axios.interceptors.request.use(config=>{
  // 为请求头添加Authorization ，从接口文档中看的
  config.headers.Authorization = sessionStorage.getItem('token')
  NProgress.start()
  return config
})

// 数据响应拦截器
axios.interceptors.response.use(res=>{
  NProgress.done()
  return res
})
// 将axios挂载到原型上
Vue.prototype.$axios = axios
Vue.component('tree-grid', ZkTable)
Vue.config.productionTip = false

// 时间过滤器
Vue.filter('formatDate',(data)=>{
  const date = new Date(data*1000)
  const y = date.getFullYear()
  const M = (date.getMonth()+1).toString().padStart(2,0)
  const d = date.getDate().toString().padStart(2,0)
  const h = date.getHours().toString().padStart(2,0)
  const m = date.getMinutes().toString().padStart(2,0)
  const s = date.getSeconds().toString().padStart(2,0)
  return `${y}-${M}-${d} ${h}:${m}:${s}`
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
